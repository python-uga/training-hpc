# Setup your environment

We will have to use Python 3 (with Miniconda3), a good Python IDE (like
Spyder) and a version-control tool (Mercurial, or Git if you know it and really
like it).

In the following, we give indications about how to install these tools and how
to get the repository of this course locally on your computer. Please, if you
encounter problems, fill an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc/issues)
to explain what you did and what are the errors (please copy / paste the error
log).

Be careful to execute all the commands in the given order, read the outputs of
the commands and answer to the questions asked by the programs!

Note that you can copy/paste the commands! On Linux, you can use the middle
button of the mouse to paste the text that has been previously selected. It's
very convenient! Try to use that!

**Note for Windows users**

You can now install Ubuntu inside Windows. It is called the [Windows Subsystem
for Linux (WSL)](https://ubuntu.com/wsl). There are some specific instructions
for WSL in the file
[notes_wsl.md](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc/-/blob/master/notes_wsl.md).

#### Install Miniconda and Python

The first step is to install miniconda3 (see
[here](https://docs.conda.io/en/latest/miniconda.html)). On Linux, one just
need to execute these commands in a terminal:

```bash
# wget is used to download a file
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
# this file (Miniconda3-latest-Linux-x86_64.sh) contains bash code and has to
# be executed with the command `bash`
bash Miniconda3-latest-Linux-x86_64.sh
```

You have to answer to different questions. In particular, answer "yes" to the
last question about the initialization of conda. When it's done, close the
terminal (with `ctrl-d`) and reopen a new terminal (with `ctrl-alt-t`). The
string "(base)" should be in the line in the terminal.

If the string "(base)" is not in the line in a new terminal, you might try to
run `$HOME/miniconda3/bin/conda init` and then reopen a new terminal (with
`ctrl-alt-t`).

You have to activate the `conda-forge` conda channel with:

```bash
conda config --add channels conda-forge
```

#### Install few packages in your base conda environment

```bash
# download a requirement file
wget https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc/-/raw/master/requirements.txt
conda install mamba
# install with conda
mamba install --file requirements.txt -y
```

#### Install and setup Mercurial

To install and setup Mercurial, you can do:

```bash
pip install conda-app -U
```

Note: if you get an error, you may need to rerun the command with the option `--no-cache-dir`.

When `conda-app` has been installed, use it to install Mercurial!

```bash
conda-app install mercurial
```

This last command can take some time. **Be patient!** When it's done, close the
terminal (`ctrl-d`) and reopen a new terminal (`ctrl-alt-t`)! Then, you should
be able to run the following commands without errors:

```bash
hg version
which hg
```

The last command should reply a path containing `miniconda`. You now needs to
create a configuration file for Mercurial. You can open it with the text editor
`gedit` with the command:

```bash
gedit ~/.hgrc &
```

Fill the file with the following text. Copy/paste it, adapt it to your case and
don't forget to save the file!

```
[ui]
username=myusername <email@adress.org>
editor=nano
tweakdefaults = True

[extensions]
hgext.extdiff =
# To use Mercurial with GitHub and Gitlab
hggit =

[extdiff]
cmd.meld =
```

Warning: Mercurial **has to be correctly setup**! Since we will use [the Gitlab
instance of UGA](https://gricad-gitlab.univ-grenoble-alpes.fr), the Mercurial
extension hg-git has to be activated so the line `hggit =` in the configuration
file is mandatory.

You can check that Mercurial is correctly configured by executing `hg version
-v`.

To learn more on Mercurial, see [here](
https://fluiddyn.readthedocs.io/en/latest/mercurial_heptapod.html).

#### Clone this repository

Once everything is installed and setup, you should be able to clone the
repository of the training with:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc.git
```

Please tell me by [creating an issue in the main repository of this
course](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc/issues)
if it does not work.

Once the repository is cloned you should have a new directory `training-hpc`
(use the command `ls` to see it) and you should be able to enter into it with
`cd training-hpc`.
