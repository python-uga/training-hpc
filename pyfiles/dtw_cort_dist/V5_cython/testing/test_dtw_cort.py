import numpy as np
import dtw_cort as dt


def test_dtw_zero_if_equal():
    """ test dtw is 0 when series are equal"""
    a = np.random.random(100)
    b = np.copy(a)
    assert(dt.dtw_distance(a, b) == 0.0)

