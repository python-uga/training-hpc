import unittest

import numpy as np

import dtw_cort as dt


class TestDtwCortCython(unittest.TestCase):

    def test_cort_one_if_equal(self):
        """check cort  returns 1 for equal arrays"""
        a = np.random.random(100)
        b = np.copy(a)
        self.assertAlmostEqual(dt.cort(a, b), 1.0)

    def test_dtw_null_if_equal(self):
        """check dtw  returns 0 for equal arrays"""
        a = np.random.random(100)
        b = np.copy(a)
        self.assertAlmostEqual(dt.dtw_distance(a, b), 0.0)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDtwCortCython)
    _res = unittest.TextTestRunner(verbosity=2).run(suite)
    import sys
    sys.exit(max(len(_res.failures), len(_res.errors)))
