#!/usr/bin/env python3
"""
Test the correlation function
"""

import corr
import numpy as np
import pytest


def test_corr():
    """TODO: Docstring for test_corr.
    :returns: TODO

    """

    assert corr.corr(np.array([1, 2, 3]), np.array([1, 2, 3])) == 1.0
    assert corr.corr(np.array([-1, 0, 1]), np.array([1, 0, -1])) == -1.0
    assert corr.corr(np.random.random(10000), np.random.random(10000)) < 0.1
    with pytest.raises(Exception):
        corr.corr(np.array([1]), np.array([1, 2]))
