#!/usr/bin/env python3

import numpy as np
import math

def corr(X, Y):
    """compile correlation between X and Y

    :X: a numpy array
    :Y: another numpy array (of same size of X)
    :returns: the correlation between X and Y.
    """

    if X.shape != Y.shape:
        raise RuntimeError("X and Y should have the same size")

    mean_x = np.mean(X)
    mean_y = np.mean(Y)
    norm_x = X - mean_x
    norm_y = Y - mean_y
    return (np.sum((norm_x) * (norm_y)))/(math.sqrt(np.sum(norm_x*norm_x)*np.sum(norm_y*norm_y)))
