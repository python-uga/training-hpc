#!/usr/bin/env python3
"""
Implementation of the kadane algorithm
"""

def max_subarray(numbers):
    """Compute the maximum sub array

    :arr1: numpy array with pos and neg elements
    :returns: the sum of the max sub array
    """
    best = 0
    current_sum = 0

    for number in numbers:
        current_sum = max(0, current_sum+number)
        best = max(best, current_sum)
    if best == 0:
        return max(numbers)
    else:
        return best
