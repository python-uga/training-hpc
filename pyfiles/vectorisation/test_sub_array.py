#!/usr/bin/env python3

import numpy as np
import max_sub_array as msa

def test_max_sub_array():
    """ test max sub_array
    :returns: TODO

    """
    # only pos
    assert msa.max_subarray([1, 2, 3, 4]) == 10

    # nominal case
    assert msa.max_subarray([-1, 7, -1, 2, -3]) == 8

    # only negative
    assert msa.max_subarray([-100, -50]) == -50
